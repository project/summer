Summer 1.x for Drupal 7
Created by AzriDesign (cssdru)
http://drupal.org/project/summer

Development sites:
http://demo.azridesign.com  - D7
http://drupal.azridesign.com - D6

-------
USAGE

Install to sites/all/themes

 - COPY the 'summer' directory within sites/all/themes/ 
 - Upload and enable the theme
 - Modify as needed

-------
SUPPORT

If you have questions or problems, check the issue list before submitting a new issue: 
http://drupal.org/project/issues/summer

For general support, please refer to:
http://drupal.org/support